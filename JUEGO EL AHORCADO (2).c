#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <string.h>
#include <conio.h>
void categorias();
void ingresoCategoria (int op);
void empezarJuego (char palabras[][15], char nombre[]);
void dibujo (int intentos);

int main(){
	system("mode con cols=80 lines=25");
	categorias();
	return 0;
}
void categorias(){
	int op;
	do{
		system("cls");
		printf("\n\t\t\t\tJUEGO DEL AHORCADO\n\n");
		printf("\t\t\tCreado por: Arturo Galicia Andrade\n\n");
		printf(" CATEGORIAS\n\n 1. Frutas\n 2. Animales\n 3. Paises\n 4. Objetos\n 5. Salir del juego\n\n Ingresa La categoria a la que desea jugar: ");
		scanf("%i",&op);		
	}while(op<1 || op>5);

	if (op==1) ingresoCategoria(op);
	if (op==2) ingresoCategoria(op);
	if (op==3) ingresoCategoria(op);
	if (op==4) ingresoCategoria(op);
}


void ingresoCategoria (int op){
	char nombrecat[4][15] = {"Frutas","Animales","Paises","Objetos"};
	char frutas [10][15] = {"ZARZAMORA","LIMON","LIMA","MANZANA","GUAYABA","MANDARINA","MELOCOTON","MANGO","CIRUELA","KIWI"};
	char animales [10][15] = {"PERRO","GATO","GALLO","GALLINA","AXOLOTE","GORILA","CERDO","LIEBRE","TORTUGA","BISONTE"};
	char paises [10][15] = {"JAPON","INGLATERRA","ARGENTINA","RUSIA","BELGICA","MEXICO","CANADA","AUSTRALIA","FRANCIA","INDIA"};
	char objetos [10][15] = {"MOCHILA","FOCO","TENIS","BOCINA","CUADERNO","SILLON","MESA","CELULAR","REFRIGERADOR","ZAPATO"};
	
	switch(op){
		case 1:
			empezarJuego(frutas,nombrecat[op-1]);
			break;
		case 2:
			empezarJuego(animales,nombrecat[op-1]);
			break;
		case 3:
			empezarJuego(paises,nombrecat[op-1]);
			break;
		case 4:
			empezarJuego(objetos,nombrecat[op-1]);
			break;	
	}	
}

void empezarJuego (char palabras[][15], char nombre[]){
	int opcion,i,j,k,longitud,espacios,puntos=1200;
	char letra;
	int aciertos = 0;
	int intentos = 0;
	int ganar = 0;
	srand(time(NULL));
	
	opcion = rand() % 10;
	longitud = strlen(palabras[opcion]);
	char frase[longitud];
	
	for(i=0; i < longitud; i++){
		frase[i] = '_';
	}
	
	do{
		aciertos = 0;
		system("cls");
		printf("\n\t\t\t\tJUEGO DEL AHORCADO\n\n");
		printf("\t\t\tCreado por: Arturo Galicia Andrade\n\n");
		printf(" CATEGORIA: %s",nombre);
		printf(" \t\t\t\t\tIntentos Disponibles: %i\n\n",6-intentos,puntos);
		dibujo(intentos);
		
		printf("\n\n\n\t\t\t      ");
		for(i=0; i < longitud; i++){
			printf(" %c ",frase[i]);
		}

		
		if (intentos == 6){
			system ("color 04");
			printf("\n\n PERDISTE!!\n LA SOLUCION ERA: %s\n\n",palabras[opcion]);
			printf(" Presiona una tecla para volver a jugar..");
			getch();
			system ("color 07");
			categorias();	
		}
		
		espacios=0;
		
		for (i = 0; i < longitud; i++){
			if (frase[i] == '_')
				espacios++;
		}	
		if (espacios == 0){
			system ("color 02");
			printf("\n\n FELICIDADES.. GANASTE!!\n\n");
			printf(" Presiona una tecla para volver a jugar..");
			getch();
			system ("color 07");
			categorias();		
		}
		
		
		printf("\n\n\n Digite una letra (EN MAYUSUCULA): ");
		scanf(" %c",&letra);
		
		for (j = 0; j < longitud; j++){
			if (letra == palabras[opcion][j]){
				frase[j] = letra;
				aciertos++;
			}	
		}
		
		if (aciertos == 0){
			intentos++;	
			puntos -= 200;
		}
	}while(intentos != 7);
	
	printf("\n\n");		
}

void dibujo (int intentos){
	switch(intentos){
		case 0:
			printf("\n\t\t\t\t     _______\n\t\t\t\t    |       |\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t ----------");
			break;
		case 1:
			printf("\n\t\t\t\t     _______\n\t\t\t\t    |       |\n\t\t\t\t    |       0\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t ----------");
			break;
		case 2:
			printf("\n\t\t\t\t     _______\n\t\t\t\t    |       |\n\t\t\t\t    |       0\n\t\t\t\t    |       |\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t ----------");
			break;
		case 3:
			printf("\n\t\t\t\t     _______\n\t\t\t\t    |       |\n\t\t\t\t    |       0\n\t\t\t\t    |      /|\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t ----------");
			break;
		case 4:
			printf("\n\t\t\t\t     _______\n\t\t\t\t    |       |\n\t\t\t\t    |       0\n\t\t\t\t    |      /|");
			printf("\\");
			printf("\n\t\t\t\t");
			printf("    |\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t ----------");
			break;
		case 5:
			printf("\n\t\t\t\t     _______\n\t\t\t\t    |       |\n\t\t\t\t    |       0\n\t\t\t\t    |      /|");
			printf("\\");
			printf("\n\t\t\t\t");
			printf("    |      /\n\t\t\t\t    |\n\t\t\t\t    |\n\t\t\t\t ----------");
			break;
		case 6:
			printf("\n\t\t\t\t     _______\n\t\t\t\t    |       |\n\t\t\t\t    |       0\n\t\t\t\t    |      /|");
			printf("\\");
			printf("\n\t\t\t\t");
			printf("    |      / ");
			printf("\\");
			printf("\n\t\t\t\t");
			printf("    |\n\t\t\t\t    |\n\t\t\t\t ----------");
			break;
	}
	
}


